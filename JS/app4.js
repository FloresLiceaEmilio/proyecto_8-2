function hacerPeticion(){
    const http = new XMLHttpRequest;
    const idAlb = document.querySelector('#input').value;
    const url = "https://jsonplaceholder.typicode.com/users/"+idAlb;

    //VALIDAR LA RESPUESTA

    http.onreadystatechange = function(){
       

        if(this.status == 200 && this.readyState == 4){
            //aqui se dibuja la pagina
            let res = document.getElementById("datos");
            const json = JSON.parse(this.responseText);
            //crear elementos 
            const tr = document.createElement('tr');
            const tdid = document.createElement('td');
            const tdname = document.createElement('td');
            const tdUserName = document.createElement('td');
            const email = document.createElement('td');
            const street = document.createElement('td');
            const suite = document.createElement('td');
            const city = document.createElement('td');
            const zipcode = document.createElement('td');
            const lat = document.createElement('td');
            const lng = document.createElement('td');
            //insertar texto
            tdid.innerText = json.id;
            tdname.innerText = json.name;
            tdUserName.innerText = json.username;
            email.innerText = json.email;
            street.innerText = json.address.street;
            suite.innerText = json.address.suite;
            city.innerText = json.address.city;
            zipcode.innerText = json.address.zipcode;
            lat.innerText = json.address.geo.lat;
            lng.innerText = json.address.geo.lng;
            tr.appendChild(tdid)
            tr.appendChild(tdname)
            tr.appendChild(tdUserName)
            tr.appendChild(email)
            tr.appendChild(street)
            tr.appendChild(suite)
            tr.appendChild(city)
            tr.appendChild(zipcode)
            tr.appendChild(lat)
            tr.appendChild(lng)

            res.appendChild(tr)

        }else if(this.readyState == 4){
            alert("No existe ese numero");
        }


    }

    http.open('GET', url,true);
    http.send();

}
//codificar los botones

document.getElementById("btnCons").addEventListener("click",function(){
    hacerPeticion();


});

